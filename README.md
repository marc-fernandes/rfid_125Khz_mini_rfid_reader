# Installation / Usage
The script is written for python3 and tested with a raspberry pi 2.

1. First pyserial must be installed. 
  * Please follow the installation instructions for pyserial at [http://pyserial.readthedocs.io](http://pyserial.readthedocs.io/en/latest/pyserial.html#installation).
2. Download this code to your raspberry pi.
3. Connect your 125 Khz rfid mini modul to your raspberry pi.
![RFID-Wiring_Steckplatine](/uploads/c4d918cec1e18721d77ff1d7bc2f0e4b/RFID-Wiring_Steckplatine.png)
4. Start the script and test it by placing some tags over the antenna.
  * ```sudo python3 rfid_reader.py```
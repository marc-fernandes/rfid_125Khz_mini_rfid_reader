#!/usr/bin/python3
import serial
import sys

def checkbit_validation(data):
    if(len(data)==5 and data[4]==data[0]^data[1]^data[2]^data[3]):
        print("Data is valid")
        return True
    else:
        print("Checkbit validation failed")
        return False

def read_rfid():
    #UART Interface: For Raspberry Pi 2 connect TxD of rfid Module to UART RxD of raspberry -- Pin 10 [GPIO 16 RxD(UART)]
    ser=serial.Serial("/dev/ttyAMA0")
    ser.baudrate=9600
    ser.stopbits=1

    #5 Bytes are transmitted with 125Khz Mini RFID
    data=ser.read(5)
    ser.close()
    return data

def calc_cardnumber(data):
    #calculate cardnumber
    sum=0
    sum=sum+data[0]
    sum=sum<<24
    sum=sum+data[1]
    sum=sum<<16
    sum2=0
    sum2=sum2+data[2]
    sum2=sum2<<8
    sum2=sum2+data[3]
    sum=sum+sum2
    # sum holds the number of the card
    return sum

print('Starte RFID-Reader ...')
try:
    while True:
        data=read_rfid()
        checkbit_validation(data)
        print("Cardnumber: " + str(calc_cardnumber(data)))
except KeyboardInterrupt:
    sys.exit() #Stop program with CTRL-C